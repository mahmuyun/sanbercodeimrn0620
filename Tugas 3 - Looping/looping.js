//SOAL NO. 1 LOOPING MENGGUNAKAN WHILE
console.log("=========================No 1 Looping While=======================");
console.log("LOOPING PERTAMA");
var count = 2;
while(count<=20){
	console.log(count + " - I Love Coding");
	count += 2;
}

console.log("LOOPING KEDUA");
var count = 20;
while(count>0){
	console.log(count + " - I will become a mobile developer");
	count -=2;
}

//SOAL NO. 2 LOOPING MENGGUNAKAN FOR
console.log("=========================No 2 Looping For=======================");
for(var i = 1; i<=20; i++){
	if (i%3 == 0 && i%2 == 1) {
       console.log(i + " - I love coding");
	}else if(i%2 == 1){
		console.log(i + " - Santai");
	}else{
		console.log(i + " - Berkualitas");
	}
}


//SOAL NO. 3 MEMBUAT PERSEGI PANJANG
console.log("=========================No 3 Membuat Persegi Panjang=======================");

var hasil3 = '';
for (var i=0; i<4; i++){
	for(var j = 0; j <8; j++){
		hasil3 += '#';
	}
	hasil3 += '\n';
}

console.log(hasil3);

//SOAL NO. 4 MEMBUAT TANGGA
console.log("=========================No 4 Membuat Tangga=======================");

let piramid = 7;
let hasil4 = '';

for(let k = 0; k< piramid; k++){
	for(let l=0; l<= k; l++){
		hasil4 += '#';
	}
	hasil4 += '\n';
}
console.log(hasil4);

//SOAL NO. 5 MEMBUAT PAPAN CATUR
console.log("=========================No 5 Membaut Papan Catur=======================");

var board = "";
for(var i = 0; i < 8; i++){
 for(var a = 0; a < 8; a++){
  if((a%2) == (i%2)){
  	board += " ";
  }else{
  	board +="#";
  }
 }
 board += "\n";
}
console.log(board);