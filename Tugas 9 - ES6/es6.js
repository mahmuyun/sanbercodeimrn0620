console.log("Soal Pertama ===========================================")

const golden = () => console.log("this is golden!!")
 
golden()

console.log("Soal Kedua ===========================================")

const newFunction = function literal(firstName, lastName){
  return {
    firstName,
    lastName,
    fullName: function(){
      console.log(`${this.firstName} ${this.lastName}`)
      return 
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName() 


console.log("Soal Ketiga ===========================================")

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation } = newObject;
console.log(firstName, lastName, destination, occupation)


console.log("Soal Keempat ===========================================")

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)


console.log("Soal Kelima ===========================================")

const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
// Driver Code
console.log(before) 

 
