function arrayToObject(arr) {
    if(arr.length==0){
        console.log(" ");
    }else{
    var now = new Date()
    var thisYear = now.getFullYear()
    var newObject = {}
    for(i=0; i<arr.length; i++){
            console.log(arr[i][0] +" "+ arr[i][1] + " :")
            newObject.firstName = arr[i][0];
            newObject.lastName = arr[i][1];
            newObject.gender = arr[i][2];
            if(thisYear-arr[i][3]<0||!arr[i][3]){
                newObject.age = "Invalid Birth year";
            }else{newObject.age = thisYear - arr[i][3];}
            
            console.log(newObject)
    }
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        g ender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""


function shoppingTime(memberId, money) {
  if(!memberId){
    return "Mohon maaf, toko x hanya berlaku untuk member saja"
  }else if(money<50000) {
    return "Mohon maaf uang tidak cukup"
  }else{
    var member = {};
    member.memberId = memberId;
    member.money = money;
    
    var list = []
    while(money>=50000){


    if(money >= 1500000){
        list.push("Sepatu Stacattu")
        money -=1500000
    }else if(money >= 500000){
        list.push("Baju Zoro")
        money -=500000
    }else if(money >= 250000){
        list.push("Baju H&N")
        money -=250000
    }else if(money >= 175000){
        list.push("Sweater Uniklooh")
        money -=175000
    }else if(money >= 50000){
        list.push("Casing Handphone")
        money -=50000
        break;
    }


    }

    member.listPurchased = list
    member.changeMoney = money;
    
    
    return member;
  }
}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// function naikAngkot(arrPenumpang) {
//     if(!arrPenumpang){
//         return [];
//     }else{

//   rute = ['A', 'B', 'C', 'D', 'E', 'F'];
//   for(i=0; i<arrPenumpang.length;i++){
//      var awal = rute.indexOf(arrPenumpang[i][1])
//     console.log(awal)
//   }
// }
// }
 
// //TEST CASE
// console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
// //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
// console.log(naikAngkot([])); //[]