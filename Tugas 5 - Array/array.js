//Soal No. 1 (Range)
console.log("============================Soal No. 1 (Range)============================")

function range(startNum, finishNum) {
var deret1 = [];
	if(startNum<finishNum){
	 	for(i=startNum; i<=finishNum; i++){
	 		deret1.push(i);
	 	}	 	
	 }else if(startNum>finishNum){
	 	
	 	for(i=startNum; i>=finishNum; i--){
	 		deret1.push(i);
	 	}	 	
	 }else{
	 	deret1 = -1;
	 }
    return(deret1);	 
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 


//Soal No. 2 (Range with Step)
console.log("============================Soal No. 2 (Range with Step)============================")

function rangeWithStep(startNum, finishNum, step) {
var deret2 = [];
	if(startNum<finishNum){
	 	for(i=startNum; i<=finishNum; i = i + step){
	 		deret2.push(i);
	 	}	 	
	 }else if(startNum>finishNum){
	 	
	 	for(i=startNum; i>=finishNum; i = i - step){
	 		deret2.push(i);
	 	}	 	
	 }else{
	 	deret2 = -1;
	 }
    return(deret2);	 
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

//Soal No. 3 (Sum of Range)
console.log("============================Soal No. 3 (Sum of Range)============================")

function sum(startNum, finishNum=0, step=1) {
var deret3 = [];

	if(startNum<finishNum){
	 	for(i=startNum; i<=finishNum; i = i = i + step){
	 		deret3.push(i);
	 	}	 	
	 }else if(startNum>finishNum){
	 	
	 	for(i=startNum; i>=finishNum; i = i - step){
	 		deret3.push(i);
	 	}	 	
	 }
	 var jumlah = 0;
    for(k=0; k<deret3.length; k++){
    	jumlah += deret3[k];
    }
   return(jumlah);


}

console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


//Soal No. 4 (Array Multidimensi)
console.log("============================Soal No. 4 (Array Multidimensi)============================")

var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] 

function dataHandling(data){
	for(i=0 ; i < data.length; i++){
		for(j=0 ; j< data[i].length; j++){
		  if(j == 0){
		  	var id = data[i][j]
		  }else if(j == 1){
		  	var nama = data[i][j]
		  }else if(j == 2){
		  	var ttl = data[i][j]
		  }else if(j == 3){
		  	var tgl = data[i][j]
		  }else{ 
		  	var hobi = data[i][j]}
		}
		console.log("nomor ID : " + id +"\nNama : " +nama +"\nTTL : "+ ttl +" "+ tgl + "\nhobi : "+hobi)
		console.log("\n")
	}
}

dataHandling(input);

// Nomor ID:  0001
// Nama Lengkap:  Roman Alamsyah
// TTL:  Bandar Lampung 21/05/1989
// Hobi:  Membaca
 
// Nomor ID:  0002
// Nama Lengkap:  Dika Sembiring
// TTL:  Medan 10/10/1992
// Hobi:  Bermain Gitar
 
// Nomor ID:  0003
// Nama Lengkap:  Winona
// TTL:  Ambon 25/12/1965
// Hobi:  Memasak
 
// Nomor ID:  0004
// Nama Lengkap:  Bintang Senjaya
// TTL:  Martapura 6/4/1970
// Hobi:  Berkebun 




//Soal No. 5 (Balik Kata)
console.log("============================Soal No. 5 (Balik Kata)============================")

function balikKata(kata){
	var arrkata = [];
	for(i=kata.length -1; i >= 0; i--){
		arrkata += kata[i];
	}
	return(arrkata);
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 






//Soal No. 6 (Metode Array)
console.log("============================Soal No. 6 (Metode Array)============================")

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
 

 function dataHandling2(data2){
 	data2.splice(1, 1, "Roman Alamsyah Elsharawy");
 	data2.splice(2, 1, "Provinsi Bandar Lampung");
 	data2.splice(4, 1, "Pria", "SMA Internasional Metro" );
 	console.log(data2);
 	var splitTanggal = data2[3].split("/");
 	var bulan = splitTanggal[1];
 	var namaBulan = "";

 	switch(bulan){
 		case '01': namaBulan = "Januari";
 		break;
 		case '02': namaBulan = "Februari";
 		break;
 		case '03': namaBulan = "Maret";
 		break;
 		case '04': namaBulan = "April";
 		break;
 		case '05': namaBulan = "Mei";
 		break;
 		case '06': namaBulan = "Juni";
 		break;
 		case '07': namaBulan = "Juli";
 		break;
 		case '08': namaBulan = "Agustus";
 		break;
 		case '09': namaBulan = "September";
 		break;
 		case '10': namaBulan = "Oktober";
 		break;
 		case '11': namaBulan = "Nopember";
 		break;
 		case '12': namaBulan = "Desember";
 		break;
 		default: namaBulan = "tidak ada nama";
 		break;
 	}
 	
   console.log(namaBulan); 
   var sortSplit = (splitTanggal.slice().sort(function(value1,value2){return value2-value1}));
   console.log(sortSplit);
   var joinSplit = (splitTanggal.join("-"));
   console.log(joinSplit);
   console.log(data2[1].slice(0,15));

 }

 dataHandling2(input);
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 