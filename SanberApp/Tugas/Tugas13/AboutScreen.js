import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, TextInput, Image, TouchableOpacity } from 'react-native';



export default function Login() {
  return (
    <View style={styles.container}> 
      <View style={styles.atas}>
      <Image source={require('./images/Group214.png')} style={styles.imageFoto}/>         
        <Text style={styles.textAccount}>Mahmud Yunus</Text>
      </View>

      <View style={styles.bawah}>
          <View style={styles.atasdua}>
          <Image source={require('./images/facebook.png')} style={styles.imageicon}/>         
          <Image source={require('./images/twitter.png')} style={styles.imageicon}/>
          <Image source={require('./images/instagram.png')} style={styles.imageicon}/>
          </View>

        <View style={styles.bawahdua}>
          <View style={styles.imagebawah}>
        <Image source={require('./images/logo-github.png')} style={styles.imageicon2}/>
        <Text style={styles.textbawah}>@younose</Text>
        </View>
        <View style={styles.imagebawah}>
        <Image source={require('./images/Vectorgitlab.png')} style={styles.imageicon2}/>
        <Text style={styles.textbawah}>@mahmuyun</Text>
        </View>

        </View>
      </View>

      <StatusBar barStyle = "light-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {false}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
       
  },
  atas:{
    flex: 2,
    backgroundColor: 'green',
    alignItems: 'center',
    justifyContent: 'center',
  },
  bawah: {
    flex: 5 ,
    backgroundColor: 'white',
  },
  atasdua: {
    flex: 2,
    backgroundColor: '#2E8747',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  bawahdua: {
    flex: 5 ,
    backgroundColor: 'white',
    padding: 30,
    borderRadius: 50
  },
  textAccount: {
      fontSize: 20,
      fontWeight: 'bold'
  },
  imageFoto: {
      marginVertical: 15,
      height: 110
  },
  imageicon: {
    height : 40,
    width: 40
  }, 
  imageicon2: {
    height: 100,
    width: 110
  },
  imagebawah:{
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginBottom: 40
  },
  textbawah:{
    fontSize: 30,
    fontWeight: 'bold',
    marginLeft: 15
  }
});
