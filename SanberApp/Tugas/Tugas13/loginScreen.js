import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, TextInput, Image, TouchableOpacity } from 'react-native';



export default function Login() {
  return (
    <View style={styles.container}> 
      <Image source={require('./images/logosanber.png')} style={{height: 97}}/>         
      <Text style={styles.textAccount}>Account Login</Text>
      <View style={styles.email}>
      <Text>Email</Text>
      </View>
      <View style={styles.password}>
      <Text>Password</Text>
      </View>
      <TouchableOpacity>
      <View style={styles.login}>
        
           <Text style={styles.textLogin}>Login</Text>
          
     
      </View>
      </TouchableOpacity>
      <Text style={styles.forgotPass}>Forgot Password</Text>
      <TouchableOpacity>
      <View style={styles.loginGoogle}>
      <Text style={styles.textLoginGoogle}>Login with Google</Text>
      </View>
      </TouchableOpacity>
      <Text>Not Have An Account</Text>
      <TouchableOpacity>
      <View style={styles.signUp}>
      <Text style={styles.textSignUp}>Sign Up Now</Text>
      </View>
      </TouchableOpacity>


      <StatusBar barStyle = "light-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {false}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'    
  },
  textAccount: {
    fontSize: 20,
    fontWeight: 'bold',
    marginVertical: 50
  },
  
  email: {
    height: 50,
    borderWidth: 2,
    borderRadius: 25,
    width: 350,
    marginBottom: 23,
    paddingLeft: 15,
    justifyContent: 'center'
  },

  password: {
    height: 50,
    borderWidth: 2,
    borderRadius: 25,
    width: 350,
    marginBottom: 23,
    paddingLeft: 15,
    justifyContent: 'center'
  },

  login: {
    height: 50,
    borderWidth: 2,
    borderRadius: 25,
    width: 350,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00ffdd',
  },

  loginGoogle: {
    height: 50,
    borderWidth: 2,
    borderRadius: 25,
    width: 350,
    marginBottom: 23,
    alignItems: 'center',
    justifyContent: 'center'
  },

  signUp: {
    height: 50,
    borderWidth: 2,
    borderRadius: 25,
    width: 350,
    marginBottom: 23,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00BCD4',
  },
  forgotPass:{
    marginBottom: 50,
  },
  textLogin: {
    fontSize: 15,
    fontWeight: 'bold',
  },
  textLoginGoogle: {
    fontSize: 15,
    fontWeight: 'bold',
  },
  textSignUp: {
    fontSize: 15,
    fontWeight: 'bold',
  },
  
});
